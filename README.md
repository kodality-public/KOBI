# KOBI

This is the information and configuration repository for a simple BI system using open source components.

Please see [Pinot](manuals/pinot.md), [Superset](manuals/superset.md), and [Podman and Docker-compose](manuals/podman-docker-compose.md) for detailed installation and configuration instructions.
