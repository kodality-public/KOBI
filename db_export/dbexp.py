import psycopg2
import sys
import datetime
import itertools

t_host = "localhost" # either a domain name, an IP address, or "localhost"
t_port = "5433" # This is the default postgres port
t_dbname = "accounting"
t_user = "postgres"
t_pw = "<password>"
t_query = "income_dev_query.sql"
t_salt = "aff9642e2db3"
t_multiplier = "0.54"
t_timeformat = "%Y-%m-%d"
t_output_prefix = "output_income_dev_"
t_tenant = '1'

def date_range(start, end):
    start = datetime.datetime.strptime(start, t_timeformat)
    end = datetime.datetime.strptime(end, t_timeformat)
    return [(start + datetime.timedelta(days=x)) for x in range(0, (end-start).days)]

def general_chunks(l, t_chunk_size):
    if t_chunk_size == 'by-months':
        return month_chunks(l)
    else:
        n = int(t_chunk_size)
        return chunks(l, n)

def chunks(l, n):
    n = max(1,n)
    starts = list(range(0, len(l), n))
    ends = [x-1 for x in starts[1:]] + [len(l)-1]
    return [(l[x], l[y]) for x, y in zip(starts, ends)]

def month_chunks(l):
    return [(x[0], x[-1]) for x in [list(g) for k, g in itertools.groupby(l, lambda d: d.month)]]

def format_times(l):
    return [(x.strftime(t_timeformat), y.strftime(t_timeformat)) for x, y in l]

def csv_export(start_date, end_date):
    print("Exporting " + start_date + " to " + end_date)
    qf = open(t_query, "r")

    s = qf.read()

    qf.close()

    s = s.replace('%salt', t_salt)
    s = s.replace('%multiplier', t_multiplier)
    s = s.replace('%start_date', start_date)
    s = s.replace('%end_date', end_date)
    s = s.replace('%tenant', t_tenant)

    #print("Executing query")
    #print(s)

    # Use the COPY function on the SQL we created above.
    SQL_for_file_output = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(s)

    # Set up a variable to store our file path and name.
    t_path_n_file = t_output_prefix + start_date + "_to_" + end_date + ".csv"

    # Trap errors for opening the file
    try:
        with open(t_path_n_file, 'w') as f_output:
            db_cursor.copy_expert(SQL_for_file_output, f_output)
    except psycopg2.Error as e:
        print("Error")
        print(e)

    # Success!

if len(sys.argv) < 3:
    print("Usage: python dbexp.py <start_date> <end_date> [chunk_size=1] [chunk_size=by-months]")
    print("E.g.: python dbexp.py 2021-01-01 2021-03-01 5")
    exit()

if len(sys.argv) < 4:
    t_chunk_size = 1
else:
    t_chunk_size = sys.argv[3]

list_to_process = format_times(general_chunks(date_range(sys.argv[1], sys.argv[2]), t_chunk_size))

if len(sys.argv) >= 5 and sys.argv[4] == 'only-print-chunks':
    print(list_to_process)
    quit()

db_conn = psycopg2.connect(host=t_host, port=t_port, dbname=t_dbname, user=t_user, password=t_pw)
db_cursor = db_conn.cursor()

for start_date, end_date in list_to_process:
   csv_export(start_date, end_date)

# Clean up: Close the database cursor and connection
db_cursor.close()
db_conn.close()
