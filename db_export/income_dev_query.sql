with s as (select
	'%salt' as salt,
	%multiplier as multiplier
	)
select
SUBSTR(md5(r.ref_practitioner_name || s.salt), 5, 12) ref_practitioner_name,
SUBSTR(r.ref_practitioner_code, 1, 1) || abs(('x' || substr(md5(r.ref_practitioner_code || s.salt), 1,8))::bit(32)::int) ref_practitioner_code,
SUBSTR(md5(r.perf_practitioner_name || s.salt), 5, 12) perf_practitioner_name,
SUBSTR(r.perf_practitioner_code, 1, 1) || abs(('x' || substr(md5(r.perf_practitioner_code || s.salt), 1,8))::bit(32)::int) perf_practitioner_code,
r.ref_organization_name,
r.ref_organization_code,
r.perf_organization_name,
r.perf_organization_code,
SUBSTR(md5(r.consumer_name || s.salt), 1, 7) consumer_name,
r.transaction_number,
r.period,
r.episode_type,
r.episode_type_text,
r.group_name item_group_text,
r.item_text,
r.source_of_finance,
r.service_code,
r.quantity,
r.document_date,
round(r.forecast_amount*s.multiplier, 2) forecast_amount,
round(r.invoice_amount*s.multiplier, 2) invoice_amount
from (with t as (
   select il.invoice_id, i.transaction_number,
          i.consumer ->> 'display' consumer_name, i.document_date, p.code period,
          item.id item_id, item.code item_code, coalesce(item.names ->> 'et', il.item_notes) item_text,
          il.referrer_practitioner ->> 'id' referrer_practitioner_id,
          il.referrer_organization ->> 'id' referrer_organization_id,
          il.performer_practitioner ->> 'id' performer_practitioner_id,
          il.performer_organization ->> 'id' performer_organization_id,
          tr.service_type, tr.source_of_financing,
          sum((case when il.dci='D' then 1 else -1 end) * il.quantity) as quantity,
          sum((case when il.dci='D' then 1 else -1 end) * coalesce(il.forecast_amount,il.amount)) as forecast_amount,
          sum((case when il.dci='D' then 1 else -1 end) * il.amount) as amount
     from billing.invoice i
          inner join billing.invoice_line il on il.invoice_id = i.id and il.sys_status='A'
          left outer join apm.item item on item.id = il.item_id
          inner join core.tenant t on i.tenant = t.id
          left outer join billing.invoice_treatment tr on i.id = tr.invoice_id
          left outer join general.period p on i.expense_period_id = p.id
    where i.sys_status='A'
      and t.id = '%tenant'
      and i.document_date between date('%start_date') and date('%end_date')
			group by il.referrer_practitioner ->> 'id',
						 il.performer_practitioner ->> 'id',
						il.referrer_organization ->> 'id',
						il.performer_organization ->> 'id',
				i.consumer ->> 'display', il.invoice_id, i.transaction_number, i.document_date, p.code,
				tr.service_type, tr.source_of_financing,
				item.id, item.code, coalesce(item.names ->> 'et', il.item_notes)
  ),
  inv as (
    select distinct invoice_id from t
  ),
  ep as (
     select inv.invoice_id, coalesce(eoc."type",'N/A') episode_type,
            row_number() over (partition by inv.invoice_id order by eoc.id desc) rn
       from inv
            left outer join billing.invoice_link il on il.invoice_id = inv.invoice_id
                   and il.sys_status = 'A' and il.resource_type='episode'
            left outer join ext.episode_of_care eoc on eoc.id = il.resource_id::bigint
  )
  select (select pr.name -> 'def' ->> 'text' from ext.practitioner pr where pr.id = t.referrer_practitioner_id) ref_practitioner_name,
         (select string_agg(pri.value,',') from ext.practitioner_identifier pri
           where pri.practitioner_id = t.referrer_practitioner_id
             and pri.sys_status = 'A' and pri."system"='tam'
             and pri.period @> t.document_date) ref_practitioner_code,
         (select o."name" -> 'def' ->> 'text' from ext.organization o where o.id = t.referrer_organization_id) as ref_organization_name,
         (select oi.value
            from ext.organization_identifier oi
           where oi.organization_id = t.referrer_organization_id and oi.sys_status='A'
             and oi.period @> current_date
           order by case when oi.system='cr' then 1 else 2 end
           limit 1) as ref_organization_code,
           (select pr.name -> 'def' ->> 'text' from ext.practitioner pr where pr.id = t.performer_practitioner_id) perf_practitioner_name,
         (select string_agg(pri.value,',') from ext.practitioner_identifier pri
           where pri.practitioner_id = t.performer_practitioner_id
             and pri.sys_status = 'A' and pri."system"='tam'
             and pri.period @> t.document_date) perf_practitioner_code,
         (select o."name" -> 'def' ->> 'text' from ext.organization o where o.id = t.performer_organization_id) as perf_organization_name,
         (select oi.value
            from ext.organization_identifier oi
           where oi.organization_id = t.performer_organization_id and oi.sys_status='A'
             and oi.period @> current_date
           order by case when oi.system='cr' then 1 else 2 end
           limit 1) as perf_organization_code,
         t.consumer_name, t.transaction_number, t.period,
         ep.episode_type,
				 coalesce(cc.names ->> 'et', 'N/A') episode_type_text, gr.group_name,
         t.item_code, t.item_text,
				 ccf."attributes" ->> 'esthif' source_of_finance,
				 ccs."attributes" ->> 'esthif' service_code,
         t.quantity, t.forecast_amount, t.amount invoice_amount, t.document_date
    from t
         left outer join
           (select igi.item_id, ig.names ->> 'et' as group_name,
                   row_number() over (partition by igi.item_id order by igi.id) rn
              from apm.item_group_item igi, apm.item_group ig
             where igi.sys_status='A'
               and igi.valid @> date('%end_date')
               and igi.group_id = ig.id
               and ig.sys_status = 'A'
         ) gr on t.item_id = gr.item_id and gr.rn = 1
         left outer join ep on ep.invoice_id = t.invoice_id and ep.rn = 1
         left outer join ext.codesystem_concept cc on cc.codesystem='healthcare-service-type' and cc.code=ep.episode_type
         left outer join ext.codesystem_concept ccs on ccs.codesystem='healthcare-service-type' and ccs.code=t.service_type
         left outer join ext.codesystem_concept ccf on ccf.codesystem='source-of-financing' and ccf.code=t.source_of_financing
   order by 1 nulls last, 3 nulls last, t.consumer_name nulls last, t.transaction_number nulls last, gr.group_name, t.item_code, t.item_text) r
cross join s
