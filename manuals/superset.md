# Superset configuration

# Management

## Users and roles

1. A user having role "Admin", can create new users.
There is a default admin user. Change its password as follows:
```
docker exec -it superset_app /bin/bash
superset fab reset-password --username admin --password yourpassword
```

2. A new user must have role `Gamma` and any additional roles per the object they may access.
For example: Roles could be `[Gamma, Income-dev-dashboarder]`.
3. All users with the `Gamma` role can access public dashboards.

## LDAP
If you have LDAP authentication enabled, the old admin user login will not work (by default).

The recommended way to proceed is to enable LDAP authentication. Login with the needed admin user via LDAP (a user will be automatically created in superset). Then switch back to the default authentication. Login as Admin and give the Admin role to the new user. Then again switch to the LDAP authentication. (There does not seem to be a way to give an existing user new roles via a command line).

## Datasets
An Alpha or Admin user can add new datasets.

# Configuration

The folder `kobi/conf/superset` is a copy from `superset/docker` configuration folder. So most of the configuration is done `kobi/conf/superset/pythonpath_dev/superset_config_docker.py` and
`kobi/conf/superset/requirements-local.txt`, which would install missing pythong packages.

For LDAP, we also added a way to add distro packages via `kobi/conf/superset/packages-local.txt`.
This should be used sparingly. The preferred way should be publishing a new superset image with packages already preinstalled (to be done).

## Setting connection to pinotdb
Docs:
https://superset.apache.org/docs/databases/docker-add-drivers

In short:
1. Add a line `pinotdb` to `docker/requirements-local.txt`.
2. Rerun the containers.
3. Databases->Add->Apache Pinot:
`pinot://<pinot-broker-host>:<pinot-broker-port>/query?controller=http://<pinot-controller-host>:<pinot-controller-port>/`

In the case, when pinot is running in a `pinot` container this should look like:
`pinot://pinot:8000/query?controller=http://pinot:9000/`

## LDAP
1. Add `python-ldap` to `docker/requirements-local.txt`.
2. Add `python3-dev libldap2-dev libsasl2-dev ldap-utils` to `docker/packages-local.txt`.
3. See `kobi/conf/superset/pythonpath_dev/superset_config_local.example` on how to enable LDAP authentication.
<details>
<summary>LDAP config</summary>

  ```
  AUTH_TYPE = 2
  AUTH_USER_REGISTRATION = True
  AUTH_USER_REGISTRATION_ROLE = "Public"
  AUTH_LDAP_UID_FIELD="sAMAccountName"
  AUTH_LDAP_FIRSTNAME_FIELD="givenName"
  AUTH_LDAP_LASTTNAME_FIELD="sn"
  AUTH_LDAP_SERVER = "ldap://ldap-server:3389"
  AUTH_LDAP_SEARCH = "<LDAP_SEARCH>"
  AUTH_LDAP_BIND_USER = "<LDAP_BIND_USER>"
  AUTH_LDAP_BIND_PASSWORD = "<LDAP_BIND_PASSWORD>"
  AUTH_LDAP_USE_TLS = False
  ```
</details>
4. Rerun the containers.
