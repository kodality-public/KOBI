# Pinot

## Overview
The docker compose file `kobi/docker-compose.yml` will start
one container with with all the pinot cluster instances (server, controller, broker)
and another container for zookeeper (where the cluster management data will be held).

## Configuration

You can configure the cluster name in `kobi/conf/pinot/*.conf`
and in the service definition in the `docker-compose.yml`.

You can also manage the jvm memory requirements with `-Xms` and `-Xmx` flags
in the docker-compose file.

## Batch ingestion
Docs:
https://docs.pinot.apache.org/basics/data-import/batch-ingestion

In short:

1) Define a schema and table configs.
https://docs.pinot.apache.org/basics/getting-started/pushing-your-data-to-pinot
You can upload a schema via Swagger Api (POST to `schemas` and `tables`).

See examples `examples/income_dev_schema.yml` and `examples/income_dev.table.yml`.


2)
In folder `data/pinot` (mapped to `/data` in the container),
create a separate folder for each job and add a job description file there.
For example, see `/data/income_dev/income_dev_in_job.xml`.
In the file you specify the input dir for the rawdata and the glob pattern,
as well as the output dir, e.g.:
```
inputDirURI: '/data/income_dev/rawdata/'
includeFileNamePattern: 'glob:**/*.csv'
outputDirURI: '/data/income_dev/segments/'
```

3)
Put your batch rawdata csv files exported from the db in the `rawdata` dir.
Pinot will ingest it automatically.

4)
For the above part you can use the script in `db_export/dbexp.py`, which
will export the provided query results in time chunks of a given size (e.g., day, week, 10 days),
depending on the size of the db. Daily chunks are completely normal, for example.

See the example: `db_export/income_dev_query.sql`.

5) Copy the data files to `data/pinot/` to the job's folder, e.g.: `data/pinot/income_dev/rawdata/`.
This can be automated via a cron job.

6) In the pinot container, run:
`bin/pinot-admin.sh LaunchDataIngestionJob -jobSpecFile /data/income_dev/income_dev_in_job.yml`,
which will ingest new data files.
(To enter the container, run:
  `podman exec -ti kobi_pinot_1 /bin/bash` or `docker exec -ti kobi_pinot_1 /bin/bash`
)
This can also be automated via a cron job.
