
If the system comes with podman instead of docker it is still possible
to use docker-compose 1.x with it.

See https://fedoramagazine.org/use-docker-compose-with-podman-to-orchestrate-containers-on-fedora/

In short:

1. Install docker-compose 1.x either from the package manager or using
https://docs.docker.com/compose/install/

2.
```
  systemctl enable podman.socket
  systemctl start podman.socket
```

3. Install `podman-docker`. In CentOS 8: `yum install podman-docker`.

4. To have DNS name resolution, be sure you have `dnsname` podman plugin installed.
In CentOS 8 this can be achieved by `yum install podman-plugins`.

5. In order for the container to be able to write to the shared local folder,
make sure you have `:z` added to the volume definition. E.g.: `./data:/data:z`.
